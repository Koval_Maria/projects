package com.deveducation.www.hw2;

public class Figures_Koval {
    public static void main(String[] args) {
        getFigure1(7);
        System.out.println();
        getFigure2(7);
        System.out.println();
        getFigure3(7);
        System.out.println();
        getFigure4(7);
        System.out.println();
        getFigure5(7);
        System.out.println();
        getFigure6(7);
        System.out.println();
        getFigure7(7);
        System.out.println();
        getFigure8(7);
        System.out.println();
        getFigure9(7);
        System.out.println();
    }

    public static void getFigure1(int a) {
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print("*  ");
            }
            System.out.println("");
        }
    }

    public static void getFigure2(int a) {
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if (i == 0 || i == 6 || j == 0 || j == 6) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println("");
        }
    }


    private static void getFigure3(int a) {
        int temp = 6;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if (i == 0 || j == 0 || j == temp) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            temp--;
            System.out.println();
        }
    }

    private static void getFigure4(int a) {
        int temp = 0;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if ((i == 0 && j == 0) || i == a - 1 || j == 0 || j == temp) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            temp++;
            System.out.println();
        }
    }

    private static void getFigure5(int a) {
        int temp = a - 1;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if (i == a - 1 || j == temp || j == a - 1) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            temp--;
            System.out.println();
        }
    }

    private static void getFigure6(int a) {
        int temp = 0;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if (i == 0 || j == a - 1 || j == temp) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            temp++;
            System.out.println();
        }
    }

    private static void getFigure7(int a) {
        int temp = 0;
        int temp2 = a - 1;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if (i == temp && j == temp || i == temp && j == temp2) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            temp++;
            temp2--;
            System.out.println();
        }
    }

    private static void getFigure8(int a) {
        int temp = 0;
        int temp2 = a - 1;
        for (int i = 0; i < (a + 1) / 2; i++) {
            for (int j = 0; j < a; j++) {
                if (i == 0 && j <= temp2 || i == temp && j == temp || i == temp && j == temp2) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            temp++;
            temp2--;
            System.out.println();
        }
    }

    private static void getFigure9(int a) {
        int temp = 0;
        int temp2 = a - 1;
        int temp3 = a / 2;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if (i == a - 1 || j == temp && i >= temp3 || j == temp2 && i >= temp3) {
                    System.out.print("*  ");
                } else {
                    System.out.print("   ");
                }
            }
            temp++;
            temp2--;
            System.out.println();
        }
    }
}

