package com.deveducation.www.hw4;

public class Customer extends Student {

    private String numberOfCreditCard;
    private String bankAccountNumber;

    public Customer(String id, String surname, String name, String patronymic, String address, String numberOfCreditCard, String bankAccountNumber) {
        super(id, surname, name, patronymic, address);
        this.numberOfCreditCard = numberOfCreditCard;
        this.bankAccountNumber = bankAccountNumber;
    }

    public String setNumberOfCreditCard(String numberOfCreditCard) {
        this.numberOfCreditCard = numberOfCreditCard;
        return numberOfCreditCard;
    }

    public String setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
        return bankAccountNumber;
    }

    public String getNumberOfCreditCard() {
        return numberOfCreditCard;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    @Override
    public String toString() {
        return "id - " + id + ", surname - " + surname + ", name - " + name + ", patronymic - " + patronymic + ", address - " + address + ", number of credit card - " + numberOfCreditCard + ", bank account number - " + bankAccountNumber + ".";

    }
}
