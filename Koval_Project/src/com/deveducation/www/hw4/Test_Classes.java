package com.deveducation.www.hw4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



public class Test_Classes {
    Student student = new Student("id0001", "Petrov", "Vladimir", "Borisovich", 2001, "Kharkov, Zernovaya street, 4, room 120", "0507531902", "Faculty of Computer Science", 1, "CS-1");
    Customer customer = new Customer("id0001", "Petrov", "Vladimir", "Borisovich", "Kharkov, Zernovaya street, 8", "4149 3575 3200 9716", "UANN3808840000026001234567890");
    Patient patient = new Patient("id0001", "Petrov", "Vladimir", "Borisovich", "Kharkov, Zernovaya street, 8", "0503106451", "817896", "cancer");
    Abiturient abiturient = new Abiturient("id0001", "Petrov", "Vladimir", "Borisovich", "Kharkov, Zernovaya street, 8", "0503106451", new int[]{67, 70, 82, 66, 72, 90, 71});
    Book book = new Book("id-5421984", "Glass lock. What the past hides", "Jannett Walls", "Force", 2020, 416, 233.12, "hard");
    House house = new House("id-37362", 143, 56, 1, 2, "Astakhova", "dwelling house", 50);
    Phone phone = new Phone("id0001", "Petrov", "Vladimir", "Borisovich",  "Kharkov, Zernovaya street, 4, room 120", "4149 3575 3200 9716", 1000, 300, 3921);


    @Test
    public void test_setIdStudent() {

        String actual = student.setId("10");
        String expected = student.getId();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setSurnameStudent() {

        String actual = student.setSurname("Ivanov");
        String expected = student.getSurname();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNameStudent() {

        String actual = student.setName("Pavel");
        String expected = student.getName();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPatronymicStudent() {

        String actual = student.setPatronymic("Nikolaevich");
        String expected = student.getPatronymic();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setDateOfBirthStudent() {

        int actual = student.setDateOfBirth(2000);
        int expected = student.getDateOfBirth();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setAddressStudent() {

        String actual = student.setAddress("Kharkiv, street Yaroslava Mudroho, 188");
        String expected = student.getAddress();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPhoneNumberStudent() {

        String actual = student.setPhoneNumber("0931067192");
        String expected = student.getPhoneNumber();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setFacultyStudent() {

        String actual = student.setFaculty("Applied Linguistics");
        String expected = student.getFaculty();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setCourseStudent() {

        int actual = student.setCourse(2);
        int expected = student.getCourse();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setGroupStudent() {

        String actual = student.setGroup("AL-2020");
        String expected = student.getGroup();
        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void test_setIdCustomer() {

        String actual = customer.setId("id-9826354");
        String expected = customer.getId();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setSurnameCustomer() {

        String actual = customer.setSurname("Evseevs");
        String expected = customer.getSurname();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNameCustomer() {

        String actual = customer.setName("Marina");
        String expected = customer.getName();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPatronymicCustomer() {

        String actual = customer.setPatronymic("Aleksandrovna");
        String expected = customer.getPatronymic();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setAddressCustomer() {

        String actual = customer.setAddress("Kharkiv, street Veselka, 52");
        String expected = customer.getAddress();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNumberOfCreditCardCustomer() {

        String actual = customer.setNumberOfCreditCard("4149 5107 3377 9124");
        String expected = customer.getNumberOfCreditCard();
        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void test_setBankAccountNumberCustomer() {

        String actual = customer.setBankAccountNumber("UANN3808840000026001234524960");
        String expected = customer.getBankAccountNumber();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setIPatient() {

        String actual = patient.setId("id-100254");
        String expected = patient.getId();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setSurnamePatient() {

        String actual = patient.setSurname("Stavrova");
        String expected = patient.getSurname();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNamePatient() {

        String actual = patient.setName("Elena");
        String expected = patient.getName();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPatronymicPatient() {

        String actual = patient.setPatronymic("Igorevna");
        String expected = patient.getPatronymic();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setAddressPatient() {

        String actual = patient.setAddress("Kiev, street Zelenaya, 74");
        String expected = patient.getAddress();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPhoneNumberPatient() {

        String actual = patient.setPhoneNumber("0931067192");
        String expected = patient.getPhoneNumber();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setMedicalRecordNumberPatient() {

        String actual = patient.setMedicalRecordNumber("513402");
        String expected = patient.getMedicalRecordNumber();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setDiagnosisPatient() {

        String actual = patient.setDiagnosis("asthma");
        String expected = patient.getDiagnosis();
        Assertions.assertEquals(expected, actual);
    }




    @Test
    public void test_setIdAbiturient() {

        String actual = abiturient.setId("id-9524");
        String expected = abiturient.getId();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setSurnameAbiturient() {

        String actual = abiturient.setSurname("Levkov");
        String expected = abiturient.getSurname();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNameAbiturient() {

        String actual = abiturient.setName("Yuriy");
        String expected = abiturient.getName();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPatronymicAbiturient() {

        String actual = abiturient.setPatronymic("Kirillovich");
        String expected = abiturient.getPatronymic();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setAddressAbiturient() {

        String actual = abiturient.setAddress("Kiev, street Ershova, 10");
        String expected = abiturient.getAddress();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPhoneNumberAbiturient() {

        String actual = abiturient.setPhoneNumber("0662204538");
        String expected = abiturient.getPhoneNumber();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setGradesAbiturient() {

        int [] actual = abiturient.setGrades(new int [] {80, 79, 86, 90, 81, 86, 78});
        int [] expected = abiturient.getGrades();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    public void test_setIdBook() {

        String actual = book.setId("id-952451");
        String expected = book.getId();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_seTitleBook() {

        String actual = book.setTitle("War and Peace");
        String expected = book.getTitle();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setAuthorsBook() {

        String actual = book.setAuthors("Leo Tolstoy");
        String expected = book.getAuthors();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPublishingHouseBook() {

        String actual = book.setPublishingHouse("Vintage Classics");
        String expected = book.getPublishingHouse();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setYearOfPublishingBook() {

        int actual = book.setYearOfPublishing(2003);
        int expected = book.getYearOfPublishing();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNumberOfPagesBook() {

        int actual = book.setNumberOfPages(1225);
        int expected = book.getNumberOfPages();
        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void test_setPriceBook() {

        double actual = book.setPrice(549.99);
        double expected = book.getPrice();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setCoverBook() {

        String actual = book.setCover("soft");
        String expected = book.getCover();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setIdHouse() {
        String actual = house.setId("id-26610");
        String expected = house.getId();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNumberOfFlat() {
        int actual = house.setNumberOfFlat(87);
        int expected = house.getNumberOfFlat();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test_setNumberOfFlat_LessThan_1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setNumberOfFlat(-8);
        });
    }

    @Test
    void test_setNumberOfFlat_MoreThan_2000() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setNumberOfFlat(2001);
        });
    }
    @Test
    public void test_setApartmentArea() {
        int actual = house.setApartmentArea(100);
        int expected = house.getApartmentArea();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    void test_setApartmentArea_LessThan_1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setApartmentArea(0);
        });
    }

    @Test
    void test_setApartmentArea_MoreThan_2000() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setApartmentArea(2001122);
        });
    }

    @Test
    public void test_setFloor() {
        int actual = house.setFloor(3);
        int expected = house.getFloor();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test_setFloor_LessThan_1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setFloor(-1);
        });
    }

    @Test
    void test_setFloor_MoreThan_165() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setFloor(166);
        });
    }

    @Test
    public void test_setNumberOfRooms() {
        int actual = house.setNumberOfRooms(11);
        int expected = house.getNumberOfRooms();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test_setNumberOfRooms_LessThan_1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setNumberOfRooms(-111111);
        });
    }

    @Test
    void test_setNumberOfRooms_MoreThan_20() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setNumberOfRooms(21);
        });
    }

    @Test
    public void test_setStreet() {
        String actual = house.setStreet("Matrosova");
        String expected = house.getStreet();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void test_setBuildingType() {
        String actual = house.setBuildingType("industrial building");
        String expected = house.getBuildingType();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setLifetime() {
        int actual = house.setLifetime(100);
        int expected = house.getLifetime();
        Assertions.assertEquals(expected, actual);
    }


    @Test
    void test_setLifetime_LessThan_1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setLifetime(0);
        });
    }

    @Test
    void test_setLifetime_MoreThan_3000() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            house.setLifetime(3482);
        });
    }




    @Test
    public void test_setIdPhone() {
        String actual = phone.setId("1620-3928-91112");
        String expected = phone.getId();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void test_setSurnamePhone() {
        String actual = phone.setSurname("Krasnova");
        String expected = phone.getSurname();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setNamePhone() {
        String actual = phone.setName("Anastsia");
        String expected = phone.getName();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setPatronymicPhone() {
        String actual = phone.setPatronymic("Vasilievna");
        String expected = phone.getPatronymic();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setAddressPhone() {
        String actual = phone.setAddress("Kharkiv, Mironositskaya street, 15, apt. 2");
        String expected = phone.getAddress();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_seNumberOfCreditCardPhone() {
        String actual = phone.setNumberOfCreditCard("5169 3305 2002 8017");
        String expected = phone.getNumberOfCreditCard();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setDebitPhone() {
        int actual = phone.setDebit(593);
        int expected = phone.getDebit();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void test_setCredittPhone() {
        int actual = phone.setCredit(33);
        int expected = phone.getCredit();
        Assertions.assertEquals(expected, actual);
    }

}


