package com.deveducation.www.streams;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MyStream {

//    public static void main(String[] args) {
//
//
//
//
//    }


    public void appliedLinguistics(List<Students> collectStudent) {

        List<Students> applied_linguistics = collectStudent.stream()
                .filter(s -> s.getFaculty().equals("Applied Linguistics"))
                .collect(Collectors.toList());
        System.out.println("Students of Applied Linguistics " + applied_linguistics);
    }

    public void appliedLinguistics_II_course(List<Students> collectStudent) {
        List<Students> applied_linguistics_II = collectStudent.stream()
                .filter(s -> s.getFaculty().equals("Applied Linguistics"))
                .filter(s -> s.getCourse().equals("II"))
                .collect(Collectors.toList());
        System.out.println("Students of Applied Linguistics, II course " + applied_linguistics_II);
    }

    public void appliedLinguistics_III_course(List<Students> collectStudent) {
        List<Students> applied_linguistics_III = collectStudent.stream()
                .filter(s -> s.getFaculty().equals("Applied Linguistics"))
                .filter(s -> s.getCourse().equals("III"))
                .collect(Collectors.toList());
        System.out.println("Students of Applied Linguistics, III course " + applied_linguistics_III);
    }

    public void facultyOfComputerScience_I_course(List<Students> collectStudent) {
        List<Students> faculty_of_computer_science_I = collectStudent.stream()
                .filter(s -> s.getFaculty().equals("Faculty of Computer Science"))
                .filter(s -> s.getCourse().equals("I"))
                .collect(Collectors.toList());
        System.out.println("Students of Faculty of Computer Science, I course " + faculty_of_computer_science_I);
    }

    public void facultyOfComputerScience_II_course(List<Students> collectStudent) {
        List<Students> faculty_of_computer_science_II = collectStudent.stream()
                .filter(s -> s.getFaculty().equals("Faculty of Computer Science"))
                .filter(s -> s.getCourse().equals("II"))
                .collect(Collectors.toList());
        System.out.println("Students of Faculty of Computer Science, II course " + faculty_of_computer_science_II);
    }

    public void yearOfBirthAfter_1998(List<Students> collectStudent) {
        List<Students> year_of_birth_after_1998 = collectStudent.stream()
                .filter(s -> s.getYearOfBirth() > 1998)
                .collect(Collectors.toList());
        System.out.println("Students were born after 1998 " + year_of_birth_after_1998);
    }

    public String studentOfGroup_CS_1(List<Students> collectStudent) {
        List<Students> student_of_group_CS_1 = collectStudent.stream()
                .filter(s -> s.getGroup().equals("CS-1"))
                .peek(s -> s.makeString())
                .collect(Collectors.toList());
        System.out.println(student_of_group_CS_1);
        return "k";
    }


    public void quantityOfStudentsFacultyOfComputerScience(List<Students> collectStudent) {
        long quantity_of_students_faculty_of_computer_science = collectStudent.stream()
                .filter(s -> s.getFaculty().equals("Faculty of Computer Science"))
                .count();
        System.out.println("Quantity of students Faculty of Computer Science " + quantity_of_students_faculty_of_computer_science);
    }


    public void transferToAnotherFaculty(List<Students> collectStudent) {
        Stream<Students> transfer = collectStudent.stream();
        transfer.map((s) -> {
            s.setFaculty("Faculty of Physics and Mathematics");
            return "Students transferred to another faculty: id: " + s.getId() + ", faculty: " + s.getFaculty() + ", student: " + s.getFirstName() + " " + s.getPatronymic() + " " + s.getLastName();
        }).forEach((s) -> System.out.println(s));
    }

    public void transferToAnotherGroup(List<Students> collectStudent) {
        Stream<Students> transfer = collectStudent.stream();
        transfer.map((s) -> {
            s.setGroup("PM-4");
            return "Students transferred to another group: id: " + s.getId() + ", group: " + s.getGroup() + ", student: " + s.getFirstName() + " " + s.getPatronymic() + " " + s.getLastName();
        }).forEach((s) -> System.out.println(s));
    }

    public Optional<String> reduceMethod(List<Students> collectStudent, int quantity, String separator) {
        List<String> temp = collectStudent.stream()
                .map((s) -> {
                    return "Student " + s.getLastName() + ", " + s.getFirstName() + ", " + s.getPatronymic() + " - " + s.getFaculty() + ", " + s.getGroup() + "\n";
                })
                .collect(Collectors.toList());
        Optional<String> result = temp.stream().limit(quantity).reduce((x, y) -> x + separator + y);
        return result;
    }
}

      class Students {
        private int id;
        private String firstName;
        private String lastName;
        private String patronymic;
        private int yearOfBirth;
        private String address;
        private String telephone;
        private String faculty;
        private String course;
        private String group;


        public Students(int id, String firstName, String lastName, String patronymic, int yearOfBirth, String address, String telephone, String faculty, String course, String group) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.patronymic = patronymic;
            this.yearOfBirth = yearOfBirth;
            this.address = address;
            this.telephone = telephone;
            this.faculty = faculty;
            this.course = course;
            this.group = group;
        }


        public void setId(int id) {
            this.id = id;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public void setPatronymic(String patronymic) {
            this.patronymic = patronymic;
        }

        public void setYearOfBirth(int yearOfBirth) {
            this.yearOfBirth = yearOfBirth;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public void setFaculty(String faculty) {
            this.faculty = faculty;
        }

        public void setCourse(String course) {
            this.course = course;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public int getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getPatronymic() {
            return patronymic;
        }

        public int getYearOfBirth() {
            return yearOfBirth;
        }

        public String getAddress() {
            return address;
        }

        public String getTelephone() {
            return telephone;
        }

        public String getFaculty() {
            return faculty;
        }

        public String getCourse() {
            return course;
        }

        public String getGroup() {
            return group;
        }

        public String makeString() {
            return "Students last name is " + lastName + ", first name is " + firstName + ", patronymic is " + patronymic;
        }

        @Override
        public String toString() {
            return "Students{" +
                    "id=" + id +
                    ", firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", patronymic='" + patronymic + '\'' +
                    ", yearOfBirth=" + yearOfBirth +
                    ", address='" + address + '\'' +
                    ", telephone='" + telephone + '\'' +
                    ", faculty='" + faculty + '\'' +
                    ", course='" + course + '\'' +
                    ", group='" + group + '\'' +
                    '}';
        }
    }




