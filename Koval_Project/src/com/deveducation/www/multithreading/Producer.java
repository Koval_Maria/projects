package com.deveducation.www.multithreading;


public class Producer extends Thread {
    private int count;

    Producer(int count) {
        this.count = count;
    }

    @Override
    public void run() {
        QueueOfMessage queueOfMessage = QueueOfMessage.getInstance();
        int i = 0;

        while (true) {
            try {
                System.out.println("Producer put");
                queueOfMessage.add("message " + ++i);
                sleep(1000);

            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}




