package com.deveducation.www.multithreading;


import java.util.ArrayList;


public class Data {

    public ArrayList<String> getMyData() {
        return myData;
    }

    public void setMyData(ArrayList<String> myData) {
        this.myData = myData;
    }

    private ArrayList<String> myData = new ArrayList<>();
}
