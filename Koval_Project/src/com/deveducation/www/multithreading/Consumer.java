package com.deveducation.www.multithreading;

public class Consumer extends Thread {

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println(QueueOfMessage.getInstance().get());
                System.out.println("Consumer get");
                sleep(1100);

            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
