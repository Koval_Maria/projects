package com.deveducation.www.multithreading;

import java.util.LinkedList;
import java.util.Queue;

public class QueueOfMessage {

    private Queue<String> queue = new LinkedList<>();
    private static volatile QueueOfMessage myQueue;

    private QueueOfMessage() {
    }

    public static synchronized QueueOfMessage getInstance() {
        if (myQueue == null) {
            myQueue = new QueueOfMessage();
        }
        return myQueue;
    }

    public void add(String text) {
        synchronized (this) {
            queue.add(text);
            notify();
        }
    }

    public synchronized String get() throws InterruptedException {
        String result = "";
        synchronized (this) {
            if (getQueue().size() == 0) {
                wait();
            } else {
                result = queue.poll();
            }
        }
        return result;
    }

    public Queue<String> getQueue() {
        return queue;
    }
}

