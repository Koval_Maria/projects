package com.deveducation.www.multithreading;

public class Client {
    public static void main(String[] args) throws InterruptedException {
        int count = 3;
        QueueOfMessage.getInstance();
        Thread producer = new Producer(count);


        Thread consumer = new Consumer();
        producer.start();
        consumer.start();

        producer.join();
        consumer.join();

        QueueOfMessage.getInstance().getQueue()
                .forEach(msg -> System.out.println(msg));
    }
}