package com.deveducation.www.interfacevariant.impl;


import com.deveducation.www.interfacevariant.data.User;

public interface IDataBase extends ICrud {
    String ADDRES_DB = "path";

    void selectAll();

    void connect();

    void disconnect();

    void insert(User user);
}