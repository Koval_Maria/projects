
package com.deveducation.www.interfacevariant.impl;

import com.deveducation.www.interfacevariant.data.CommonData;
import com.deveducation.www.interfacevariant.data.Note;

import java.util.List;

public interface IFile extends ICrud {
    String PATH_FILE = "C:\\Users\\Mary\\IdeaProjects\\Practice\\src\\MyFile.txt";

    void write(CommonData data);

    void write(Object note);

    void write(Note note);
    List<Note> read ();
    void openStream();
    void closeStream();
}
