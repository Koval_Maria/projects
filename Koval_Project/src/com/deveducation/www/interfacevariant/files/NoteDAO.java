package com.deveducation.www.interfacevariant.files;


import com.deveducation.www.interfacevariant.data.CommonData;
import com.deveducation.www.interfacevariant.data.Note;
import com.deveducation.www.interfacevariant.impl.IFile;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

public class NoteDAO implements IFile {
    List<Note> notes;
    public static final String FILE_NAME = "note.txt";

    @Override
    public void write(CommonData data) {
        System.out.println(data + " was written in file " + FILE_NAME);
    }

    @Override
    public void write(Object note) {

    }


    public NoteDAO() {
        this.notes = new ArrayList<>();
    }

    private FileReader fileReader;

    {
        try {
            fileReader = new FileReader(PATH_FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Writer writer = new Writer(PATH_FILE) {
        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {

        }

        @Override
        public void flush() throws IOException {

        }

        @Override
        public void close() throws IOException {

        }
    };

        @Override
        public void write(Note note) {
            this.notes.add(note);
        }


        @Override
        public List<Note> read() {
            return this.notes;
        }

        @Override
        public void openStream() {

            System.out.println("Stream was opened");
        }

        @Override
        public void closeStream() {
            System.out.println("Stream was closed");

        }

        @Override
        public void delete(int id) {
            System.out.println("Method delete from Note class was called");
        }

        @Override
        public void update(CommonData data) {
            for (int i = 0; i < this.notes.size(); i++) {
                if (data.getId() == this.notes.get(i).getId()) {
                    this.notes.set(i, (Note) data);
                    break;
                }
            }
        }

        @Override
        public void clearAll() {
            System.out.println("Method clearAll from Note class was called");
        }
    }
