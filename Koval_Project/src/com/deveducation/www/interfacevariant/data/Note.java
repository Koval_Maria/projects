package com.deveducation.www.interfacevariant.data;

public class Note extends CommonData {
    public String getMessage() {
        return message;
    }

    protected String message;

    @Override
    public String toString() {
        return "Note{" +
                "message='" + message + '\'' +
                '}';
    }

    public Note(int id, String message) {
        super(id);
        this.message = message;
    }
}
