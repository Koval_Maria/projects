package com.deveducation.www.interfacevariant.data;

public abstract class CommonData {
    protected int id;

    public CommonData(int id) {

        this.id = id;
    }

    public int getId() {
        return id;
    }
}
