package com.deveducation.www.hw3;

import java.util.Scanner;

public class SymbolsStrings_Koval {

    char[] getEnglishAlphabetCapital() {
        int size = 91 - 65;
        char[] myArray = new char[size];
        int index = 0;
        for (int i = 65; i < 91; i++) {
            myArray[index] = (char) i;
            index++;
        }
        return myArray;
    }

    char[] getEnglishAlphabetSmall() {
        int size = 123 - 97;
        char[] myArray = new char[size];
        int index = 0;
        for (int i = 97; i < 123; i++) {
            myArray[index] = (char) i;
            index++;
        }
        return myArray;
    }

    char[] getRussianAlphabetSmall() {
        int size = 1104 - 1072;
        char[] myArray = new char[size];
        int index = 0;
        for (int i = 1072; i < 1104; i++) {
            myArray[index] = (char) i;
            index++;
        }
        return myArray;
    }

    char[] getListOfNumbers() {
        int size = 58 - 48;
        char[] myArray = new char[size];
        int index = 0;
        for (int i = 48; i < 58; i++) {
            myArray[index] = (char) i;
            index++;
        }
        return myArray;
    }

    char[] getASCIISymbols() {
        int size = 127 - 33;
        char[] myArray = new char[size];
        int index = 0;
        for (int i = 33; i < 127; i++) {
            myArray[index] = ((char) i);
            index++;
        }
        return myArray;
    }

    String getIntToString(int num) {
        String str = "";
        str = Integer.toString(num);
        str = str.concat(" is your integer");
        return str;
    }


    String getDoubleToString(double num) {
        String str = "";
        str = Double.toString(num);
        str = str.concat(" is your double");
        return str;
    }

    int getStringToInt(String num) {

        int str = Integer.parseInt(num);
        str += 100;
        return str;
    }

    double getStringToDouble(String num) {

        double str = Double.parseDouble(num);
        str += 0.2511;
        return str;
    }

    int getLengthOfShortest(String text) {
        String str2 = text.replace(",", "").replace("!", "").replace("?", "").replace(":", "").replace(";", "").replace(".", "");
        String[] str = str2.split(" ");
        int temp = 30;
        for (String s : str) {
            if (s.length() < temp) {
                temp = s.length();
            }
        }
        return temp;
    }

    String[] addSymbolToWords(String[] myArray, int numberOfLetters) {
        String[] theResult = new String[myArray.length];
        int index = 0;
        for (String s : myArray) {
            if (s.length() == numberOfLetters) {
                StringBuilder sb = new StringBuilder(s);
                sb.delete(2, 5);
                sb.append("$$$");
                theResult[index++] = sb.toString();
            } else {
                theResult[index++] = s;
            }
        }

        return theResult;
    }

    String addSpaceToPunctuationMarks(String text) {
        return text.replace(",", ", ").replace("!", "! ").replace("?", "? ").replace(":", ": ").replace(";", "; ").replace("-", " - ").replace("(", " (").replace(")", ") ").replace("  ", " ").replace("   ", " ");
    }

    String getUniqueChar(String text) {

        StringBuilder theResult = new StringBuilder();
        boolean[] uniqCheck = new boolean[1104];
        for (int i = 0; i < text.length(); i++) {
            if (!uniqCheck[text.charAt(i)]) {
                uniqCheck[text.charAt(i)] = true;
                if (uniqCheck[text.charAt(i)])
                    theResult.append(text.charAt(i));
            }
        }
        return theResult.toString();
    }

    String getText(String text) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слова одной строкой через пробел:");
        String str = sc.nextLine();
        return str;
    }

    int getQuantityOfWords(String str) {
        String[] words = str.split(" ");

        int count = 0;

        if (words.length != 0) {
            for (String i : words) {
                count++;
            }
        }
        return count;
    }


    String deletePartOfString(String text, int startIdx, int endIdx) {
        StringBuilder str = new StringBuilder(text);
        str.replace(startIdx, endIdx, "");
        return str.toString();
    }

    String getReversedString(String text2) {
        StringBuilder str = new StringBuilder(text2);
        str.reverse();
        return str.toString();
    }

    String deleteLastWord(String text) {
        return text.substring(0, text.lastIndexOf(" "));
    }
}






