package com.deveducation.www.hw1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


public class Test_Loop_Koval {
    static Loop_Koval loop;

    @BeforeAll
    public static void init() {
        loop = new Loop_Koval();
    }


    @ParameterizedTest
    @CsvSource(value = {"sum = 2450, quantity = 49:99", "sum = 650, quantity = 25:50", "sum = 6, quantity = 2:4", "sum = 2450, quantity = 49:98"}, delimiter = ':')
    void test_getSumOfParityAndTheirQuantity(String expected, int actual) {
        Assertions.assertEquals(expected, loop.getSumOfParityAndTheirQuantity(actual));
    }

    @Test
    void test_getSumOfParityAndTheirQuantity_100() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Loop_Koval().getSumOfParityAndTheirQuantity(100);
        });
    }

    @Test
    void test_getSumOfParityAndTheirQuantity_0() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Loop_Koval().getSumOfParityAndTheirQuantity(0);
        });
    }

    @Test
    void test_getSumOfParityAndTheirQuantity_neg() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Loop_Koval().getSumOfParityAndTheirQuantity(-121545455);
        });
    }


    @ParameterizedTest
    @CsvSource(value = {"Prime number:1", "Composite number:10", "The number is 0:0", "The value is out of range:-5"}, delimiter = ':')
    void test_identifyPrimeNumber(String expected, int actual) {
        Assertions.assertEquals(expected, loop.identifyPrimeNumber(actual));
    }


    @ParameterizedTest
    @CsvSource(value = {"true:3", "false:120", "false:0", "false:-8"}, delimiter = ':')
    void test_identifyPrimeNumber2(boolean expected, int actual) {
        Assertions.assertEquals(expected, loop.identifyPrimeNumber2(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"2:4", "3:10", "1:1", "0:0", "0:-9"}, delimiter = ':')
    void test_getSquareRoot(int expected, int actual) {
        Assertions.assertEquals(expected, loop.getSquareRoot(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"120:5", "0:-2", "1:1", "0:0"}, delimiter = ':')
    void test_getFactorial(int expected, int actual) {
        Assertions.assertEquals(expected, loop.getFactorial(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"10:1234", "1:1"}, delimiter = ':')
    void test_getSumOfDigits(int expected, int actual) {
        Assertions.assertEquals(expected, loop.getSumOfDigits(actual));
    }

    @Test
    void test_getSumOfDigits_zero() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Loop_Koval().getSumOfDigits(0);
        });
    }

    @Test
    void test_getSumOfDigits_neg() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Loop_Koval().getSumOfDigits(-15);
        });
    }

    @ParameterizedTest
    @CsvSource(value = {"52841:14825", "1:1", "0:0", "0001:1000"}, delimiter = ':')
    void test_getMirrorNumber(String expected, int actual) {
        Assertions.assertEquals(expected, loop.getMirrorNumber(actual));
    }

    @Test
    void test_getMirrorNumber() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Loop_Koval().getSumOfDigits(-12);
        });
    }
}


