package com.deveducation.www.hw1;

public class Arrays_Koval {


    public int getMin(int[] myArray) {

        int result = myArray[0];
        for (int value : myArray) {
            if (value < result) {
                result = value;
            }
        }
        return result;

    }

    public int getMax(int[] myArray) {
        int result = myArray[0];
        for (int value : myArray) {
            if (value > result) {
                result = value;
            }
        }
        return result;
    }

    public int getIndexOfMin(int[] myArray) {
        int indexOfMin = 0;
        int result = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] < result) {
                result = myArray[i];
                indexOfMin = i;
            }
        }
        return indexOfMin;

    }

    public int getIndexOfMax(int[] myArray) {
        int indexOfMax = 0;
        int result = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] > result) {
                result = myArray[i];
                indexOfMax = i;
            }
        }
        return indexOfMax;
    }

    public int getSumOfOddIndexNumbers(int[] myArray) {
        int sum = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (i % 2 != 0) {
                sum += myArray[i];
            }
        }
        return sum;
    }

    public int[] getReversedArray(int[] myArray) {
        int[] reversed = new int[myArray.length];
        int n = 0;
        for (int i = myArray.length - 1; i >= 0; i--) {
            reversed[n] = myArray[i];
            n++;
        }
        return reversed;
    }

    public int getQuantityOfOddElements(int[] myArray) {
        int quantity = 0;
        for (int value : myArray) {
            if (value % 2 != 0) {
                quantity++;
            }
        }
        return quantity;
    }

    public int[] getReversedHalfArray(int[] myArray) {
        int[] reversed = new int[myArray.length];
        int first = 0;
        int second = myArray.length / 2;

        if (myArray.length % 2 != 0) {
            second++;
        }

        for (int i = 0; i < myArray.length; i++) {
            if (i < myArray.length / 2) {
                reversed[second] = myArray[i];
                second++;
            } else {
                if (myArray.length % 2 != 0) {
                    if (i == myArray.length / 2) {
                        reversed[myArray.length / 2] = myArray[i];
                    } else {
                        reversed[first] = myArray[i];
                        first++;
                    }
                } else {
                    reversed[first] = myArray[i];
                    first++;
                }
            }
        }
        return reversed;
    }

    public int[] getBubbleSort(int[] myArray) {
        int length = myArray.length;
        for (int i = 0; i < length - 1; i++)
            for (int j = 0; j < length - i - 1; j++)
                if (myArray[j] > myArray[j + 1]) {
                    int temp = myArray[j];
                    myArray[j] = myArray[j + 1];
                    myArray[j + 1] = temp;
                }
        return myArray;
    }

    public int[] getSelectionSort(int[] myArray) {
        for (int i = 0; i < myArray.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < myArray.length; j++) {
                if (myArray[j] < myArray[index]) {
                    index = j;
                }
            }
            int smallerNumber = myArray[index];
            myArray[index] = myArray[i];
            myArray[i] = smallerNumber;
        }
        return myArray;
    }

    public int[] getInsertSort(int[] myArray) {
        for (int left = 1; left < myArray.length; left++) {
            int value = myArray[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < myArray[i]) {
                    myArray[i + 1] = myArray[i];
                } else {
                    break;
                }
            }
            myArray[i + 1] = value;
        }
        return myArray;
    }
}