package com.deveducation.www.hw1;

public class ConditionalStatements_Koval {

    public int checkParity(int num1, int num2) {
        int result;
        if (num1 % 2 == 0) {
            result = num1 * num2;
        } else {
            result = num1 + num2;
        }
        return result;
    }


    public String getQuarterDefinition(int x, int y) {
        String theNumberOfQuarter = "";

        if (x == 0) {
            if (y > 0) {
                theNumberOfQuarter = "Between I and II";
            }
            if (y < 0) {
                theNumberOfQuarter = "Between III and IV";
            }
        }

        if ((x == 0) && (y == 0)) {
            theNumberOfQuarter = "This point is origin";
        }

        if (x > 0) {
            if (y > 0) {
                theNumberOfQuarter = "I";
            } else if (y < 0) {
                theNumberOfQuarter = "IV";
            }
        } else if (x < 0) {
            if (y > 0) {
                theNumberOfQuarter = "II";
            } else if (y < 0) {
                theNumberOfQuarter = "III";
            }
        }
        return theNumberOfQuarter;
    }

    public int getSumOfPositive(int num1, int num2, int num3) {
        int sum = 0;

        if (num1 > 0) {
            sum += num1;
        }

        if (num2 > 0) {
            sum += num2;
        }

        if (num3 > 0) {
            sum += num3;
        }

        return sum;
    }

    public int getSumOfMax(int num1, int num2, int num3) {
        int theResult = 3;
        int sum = num1 + num2 + num3;
        int compos = num1 * num2 * num3;

        if (compos >= sum) {
            theResult += compos;
        } else {
            theResult += sum;
        }

        return theResult;
    }

    public String defineTheMark(int mark) {
        String theMark = "";

        if ((mark < 0) || (mark > 100)) {
            theMark = "The integer is out of range";
        }

        if ((mark >= 0) && (mark < 20)) {
            theMark = "F";
        } else if ((mark >= 20) && (mark < 40)) {
            theMark = "E";
        } else if ((mark >= 40) && (mark < 60)) {
            theMark = "D";
        } else if ((mark >= 60) && (mark < 75)) {
            theMark = "C";
        } else if ((mark >= 75) && (mark < 90)) {
            theMark = "B";
        } else if ((mark >= 90) && (mark <= 100)) {
            theMark = "A";
        }

        return theMark;

    }
}