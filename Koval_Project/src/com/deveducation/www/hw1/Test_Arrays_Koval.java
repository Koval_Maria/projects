package com.deveducation.www.hw1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Test_Arrays_Koval {

    static Arrays_Koval arrays;
    int[] myArray = new int[]{914, -157, 1, 0, -12, 96, 1201};
    int[] myArray1 = new int[]{101, 914, -157, 1, 0, -12, 96, 1201};



    @BeforeAll
    public static void init() {
        arrays = new Arrays_Koval();

    }

    @Test

    public void test_getMin_myArray() {
        int expected = -157;
        int actual = arrays.getMin(this.myArray);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getMax_myArray() {
        int expected = 1201;
        int actual = arrays.getMax(this.myArray);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getIndexOfMin_myArray() {
        int expected = 1;
        int actual = arrays.getIndexOfMin(this.myArray);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getIndexOfMax_myArray() {
        int expected = 6;
        int actual = arrays.getIndexOfMax(this.myArray);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfOddIndexNumbers_myArray() {
        int expected = -61;
        int actual = arrays.getSumOfOddIndexNumbers(this.myArray);
        Assertions.assertEquals(expected, actual);
    }


    @Test

    public void test_getReversedArray_myArray() {
        int[] expected = {1201, 96, -12, 0, 1, -157, 914};
        int[] actual = arrays.getReversedArray(this.myArray);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getQuantityOfOddElements_myArray() {
        int expected = 3;
        int actual = arrays.getQuantityOfOddElements(this.myArray);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getReversedHalfArray_myArray() {
        int[] expected = {-12, 96, 1201, 0, 914, -157, 1};
        int[] actual = arrays.getReversedHalfArray(this.myArray);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getReversedHalfArray_myArray_1() {
        int[] expected = {0, -12, 96, 1201, 101, 914, -157, 1};
        int[] actual = arrays.getReversedHalfArray(this.myArray1);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getBubbleSort_myArray() {
        int[] expected = {-157, -12, 0, 1, 96, 914, 1201};
        int[] actual = arrays.getBubbleSort(this.myArray);
        Assertions.assertArrayEquals(expected, actual);
    }


    @Test

    public void test_getSelectionSort_myArray() {
        int[] expected = {-157, -12, 0, 1, 96, 914, 1201};
        int[] actual = arrays.getSelectionSort(this.myArray);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getInsertSort_myArray() {
        int[] expected = {-157, -12, 0, 1, 96, 914, 1201};
        int[] actual = arrays.getInsertSort(this.myArray);
        Assertions.assertArrayEquals(expected, actual);
    }
}
