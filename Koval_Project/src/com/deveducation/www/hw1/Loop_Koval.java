package com.deveducation.www.hw1;

public class Loop_Koval {

    public String getSumOfParityAndTheirQuantity(int num) {
        int sum = 0;
        int quantity = 0;

        for (int i = 1; i <= num; i++)
            if (i % 2 == 0) {
                sum += i;
                quantity++;
            }
        if (num > 99 || num < 1) {
            throw new IllegalArgumentException("The value is out of range");
        }
        return "sum = " + sum + ", quantity = " + quantity;
    }


    public String identifyPrimeNumber(int num) {
        String theResult = "Prime number";
        if (num < 0) {
            theResult = "The value is out of range";
        }
         for (int i = 0; i <= num; i++) {
            if ((num % 2) != 0) {
                theResult = "Prime number";
            } else if  (num == 0) {
                theResult = "The number is 0";
            }
            else {
                theResult = "Composite number";
            }
        }
        return theResult;
    }

    public boolean identifyPrimeNumber2(int num) { //this is the second version without redundant loop
        if (num <= 0) {
            return false;
        }
        return (num % 2) != 0;
    }

    public int getSquareRoot(int x) {
        int num = 1;
        int theResult = 0;
        while (x >= 0) {
            x -= num;
            num += 2;
            theResult += (x < 0) ? 0 : 1;
        }
        return theResult;
    }

    public int getFactorial(int num) {
        int theResult = 1;
        if (num <= 0) {
            theResult = 0;
        }
        for (int i = 1; i <= num; i++) {
            theResult *= i;
        }
        return theResult;
    }

    public int getSumOfDigits(int num) {
        int sum = 0;
        int temp;
        if (num <= 0) {
            throw new IllegalArgumentException("The digit is out of range");
        }
        while (num > 0) {
            temp = num % 10;
            sum += temp;
            num /= 10;
        }
        return sum;
    }

    public String getMirrorNumber(int num) {
        String mirror = "";
        int temp = 0;

        if (num < 0) {
            throw new IllegalArgumentException("The value is out of range");
        }
        while (num != 0) {
            temp = temp * 10 + (num % 10);
            num /= 10;
            if (temp == 0) {
                mirror = mirror + "0";
            }
        }
        return mirror.concat(Integer.toString(temp));
    }
}
