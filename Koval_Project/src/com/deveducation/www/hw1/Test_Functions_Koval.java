package com.deveducation.www.hw1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Test_Functions_Koval {

    static Functions_Koval functions;

    @BeforeAll
    public static void init() {
        functions = new Functions_Koval();
    }

    @ParameterizedTest
    @CsvSource(value = {"zero:0", "one:1", "nine:9", "ten:10", "eleven:11", "nineteen:19", "twenty:20", "twenty one:21", "thirty:30", "ninety:90", "one hundred:100", "one hundred and one:101", "one hundred and nineteen:119", "three hundred and fifty:350", "five hundred and nine:509", "nine hundred and ninety eight:998", "\t\n" +
            "nine hundred and ninety nine:999"}, delimiter = ':')
    void test_getNumbersInWords(String expected, int actual) {
        Assertions.assertEquals(expected, functions.getNumbersInWords(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"0:zero", "1:one", "9:nine", "10:ten", "11:eleven", "19:nineteen", "20:twenty", "21:twenty one", "30:thirty", "90:ninety", "100:one hundred", "101:one hundred and one", "119:one hundred and nineteen", "350:three hundred and fifty", "509:five hundred and nine", "998:nine hundred and ninety eight", "\t\n" +
            "999:nine hundred and ninety nine"}, delimiter = ':')
    void test_getWordsInNumber(int expected, String actual) {
        Assertions.assertEquals(expected, functions.getWordsInNumber(actual));
    }

    @Test
    void test_getWordsInNumber() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Functions_Koval().getWordsInNumber("thousand");
        });
    }

    @Test
    void test_getWordsInNumber1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Functions_Koval().getWordsInNumber("minus six");
        });
    }
}
