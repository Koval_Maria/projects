package com.deveducation.www.hw1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Test_ConditionalStatements_Koval {
    static ConditionalStatements_Koval cs;
    @BeforeAll
    public static void init() {
        cs = new ConditionalStatements_Koval();
    }


    @Test

    public void test_checkParity_4_3() {
        int expected = 12;
        int actual = cs.checkParity(4, 3);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_checkParity_1_4() {
        int expected = 5;
        int actual = cs.checkParity(1, 4);
        Assertions.assertEquals(expected, actual);
    }
    @Test

    public void test_checkParity_neg8_10() {
        int expected = -80;
        int actual = cs.checkParity(-8, 10);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_checkParity_neg9_neg111() {
        int expected = -120;
        int actual = cs.checkParity(-9, -111);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_checkParity_0_1() {
        int expected = 0;
        int actual = cs.checkParity(0, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_checkParity_0_0() {
        int expected = 0;
        int actual = cs.checkParity(0, 0);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_checkParity_122_0() {
        int expected = 0;
        int actual = cs.checkParity(122, 0);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_checkParity_3_neg15() {
        int expected = -12;
        int actual = cs.checkParity(3, -15);
        Assertions.assertEquals(expected, actual);
    }
    @Test

    public void test_checkParity_2146_999_999() {
        int expected = 2_145_997_854;
        int actual = cs.checkParity(2146, 999999);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_checkParity_neg2146_neg999_999() {
        int expected = 2_145_997_854;
        int actual = cs.checkParity(-2146, -999999);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getQuarterDefinition_0_6() {
        String expected = "Between I and II";
        String actual = cs.getQuarterDefinition(0, 6);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getQuarterDefinition_0_neg10() {
        String expected = "Between III and IV";
        String actual = cs.getQuarterDefinition(0, -10);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getQuarterDefinition_0_0() {
        String expected = "This point is origin";
        String actual = cs.getQuarterDefinition(0, 0);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getQuarterDefinition_9_10() {
        String expected = "I";
        String actual = cs.getQuarterDefinition(9, 10);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getQuarterDefinition_1_neg1() {
        String expected = "IV";
        String actual = cs.getQuarterDefinition(1, -1);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getQuarterDefinition_neg100_99() {
        String expected = "II";
        String actual = cs.getQuarterDefinition(-100, 99);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getQuarterDefinition_neg1_neg1548() {
        String expected = "III";
        String actual = cs.getQuarterDefinition(-100, -1548);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfPositive_2_17_199() {
        int expected = 218;
        int actual = cs.getSumOfPositive(2, 17, 199);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfPositive_0_54_1() {
        int expected = 55;
        int actual = cs.getSumOfPositive(0, 54, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfPositive_neg100_987_12() {
        int expected = 999;
        int actual = cs.getSumOfPositive(-100, 987, 12);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfPositive_neg100_neg1_neg9() {
        int expected = 0;
        int actual = cs.getSumOfPositive(-100, -1, -9);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfMax_10_1_9() {
        int expected = 93;
        int actual = cs.getSumOfMax(10, 1, 9);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfMax_1_1_1() {
        int expected = 6;
        int actual = cs.getSumOfMax(1, 1, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfMax_neg150_neg8_neg796() {
        int expected = -951;
        int actual = cs.getSumOfMax(-150, -8, -796);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfMax_neg10_neg2_9() {
        int expected = 183;
        int actual = cs.getSumOfMax(-10, -2, 9);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfMax_1000_4_neg1() {
        int expected = 1006;
        int actual = cs.getSumOfMax(1000, 4, -1);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_getSumOfMax_0_0_0() {
        int expected = 3;
        int actual = cs.getSumOfMax(0, 0, 0);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_defineTheMark_15() {
        String expected = "F";
        String actual = cs.defineTheMark(15);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_defineTheMark_39() {
        String expected = "E";
        String actual = cs.defineTheMark(39);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_defineTheMark_40() {
        String expected = "D";
        String actual = cs.defineTheMark(40);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_defineTheMark_68() {
        String expected = "C";
        String actual = cs.defineTheMark(68);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_defineTheMark_75() {
        String expected = "B";
        String actual = cs.defineTheMark(75);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_defineTheMark_100() {
        String expected = "A";
        String actual = cs.defineTheMark(100);
        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_defineTheMark_neg15() {
        String expected = "The integer is out of range";
        String actual = cs.defineTheMark(-15);
        Assertions.assertEquals(expected, actual);
    }
    @Test

    public void test_defineTheMark_101() {
        String expected = "The integer is out of range";
        String actual = cs.defineTheMark(101);
        Assertions.assertEquals(expected, actual);
    }

}
