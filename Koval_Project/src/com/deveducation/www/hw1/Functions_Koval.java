package com.deveducation.www.hw1;



public class Functions_Koval {


    protected String getNumbersInWords(int num) {
        String theResult = "";
        if (num < 0 || num > 999) {
            new IllegalArgumentException("out of range");
        }
        if (num == 0) {
            theResult = "zero";
        }

        String [] toTen = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
        String [] toTwenty = {"", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        String [] decimal = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        String [] hundred = {"hundred"};


        if (num > 0 && num <= 10) {
            return (toTen[num]);
        }
        if (num > 10 && num < 20) {
            return (toTwenty[num - 10]);
        }
        if (num == 100) {
            theResult = "one hundred";
        }
        if (num / 100 >= 1 && num / 100 <= 9) {
            if (num % 100 >= 1 && num % 100 <= 9) {
                theResult = toTen[num / 100] + " " + hundred[0] + " " + "and" + " " + toTen[num % 100];
            }
            if (num % 100 >= 10 && num % 100 <= 19) {
                theResult = toTen[num / 100] + " " + hundred[0] + " " + "and" + " " + toTwenty[num % 100 - 10];
            }
            if (num % 100 >= 20 && num % 100 <= 99) {
                if (num % 10 >= 1 && num % 10 < 10) {
                    theResult = toTen[num / 100] + " " + hundred[0] + " " + "and" + " " + decimal[num % 100 / 10 - 2] + " " + toTen[num % 10];
                } else {
                    theResult = toTen[num / 100] + " " + hundred[0] + " " + "and" + " " + decimal[num % 10 + 3];
                }

            }
        } else {
            if (num / 10 >= 2 && num / 10 <= 9) {
                theResult = decimal[num / 10 - 2];
            }
            if (num % 10 >= 1 && num % 10 <= 9) {
                theResult = decimal[num / 10 - 2] + " " + toTen[num % 10];
            }

        }
        return theResult;
    }


    protected int getWordsInNumber(String text) {
        String [] notValid = {"thousand", "million", "billion", "milliard", "minus"};
        String [] arrayWords = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        int [] arrayNumbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900};
        String [] splitText = text.split(" ");

        int theResult = 0;

        for (String s : notValid) {
            for (String value : splitText) {
                if (s.equals(value)) {
                    throw new IllegalArgumentException();
                }
            }
        }
        for (int i = 0; i < splitText.length; i++) {
            if (splitText[i].equals("hundred")) {
                theResult *= 100;
            }
            if (splitText[i] == "and") {
                splitText[i] = "";
            }

            for (int j = 0; j < arrayWords.length; j++) {

                if (splitText[i].equals(arrayWords[j])) {
                    theResult += arrayNumbers[j];
                }
            }
        }  return theResult;
    }
}



