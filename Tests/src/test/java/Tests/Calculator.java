package Tests;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class Calculator {
    static WebElement zero;
    static WebElement one;
    static WebElement two;
    static WebElement three;
    static WebElement four;
    static WebElement five;
    static WebElement six;
    static WebElement seven;
    static WebElement eight;
    static WebElement nine;
    static WebElement decimal;
    static WebElement equals;
    static WebElement add;
    static WebElement subtract;
    static WebElement multiply;
    static WebElement divide;
    static WebElement display;
    static WebElement percent;
    static WebElement neg;
    static WebElement clear;
    static WebElement openParenthesis;
    static WebElement closeParenthesis;
    static WebElement memoryRecall;
    static WebElement memorySubtract;
    static WebElement memoryAdd;
    static WebElement memoryClear;
    static WebElement square;
    static WebElement cube;
    static WebElement pow;
    static WebElement eToXPower;
    static WebElement tenToXPower;
    static WebElement inverse;
    static WebElement sqrt;
    static WebElement cbrt;
    static WebElement nthRoot;
    static WebElement ln;
    static WebElement log;
    static WebElement factorial;
    static WebElement sin;
    static WebElement cos;
    static WebElement tan;
    static WebElement e;
    static WebElement toExponential;
    static WebElement trigUnit;
    static WebElement sinh;
    static WebElement cosh;
    static WebElement tanh;
    static WebElement pi;
    static WebElement random;
    static WebElement unit;


    private static WebDriver driver;


    @BeforeClass
    public static void initDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Mary\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shvachko/scientificCalculator/index.html");
        zero = driver.findElement(By.id("zero"));
        one = driver.findElement(By.id("one"));
        two = driver.findElement(By.id("two"));
        three = driver.findElement(By.id("three"));
        four = driver.findElement(By.id("four"));
        five = driver.findElement(By.id("five"));
        six = driver.findElement(By.id("six"));
        seven = driver.findElement(By.id("seven"));
        eight = driver.findElement(By.id("eight"));
        nine = driver.findElement(By.id("nine"));
        decimal = driver.findElement(By.id("decimal"));
        equals = driver.findElement(By.id("equals"));
        add = driver.findElement(By.id("add"));
        subtract = driver.findElement(By.id("subtract"));
        multiply = driver.findElement(By.id("multiply"));
        divide = driver.findElement(By.id("divide"));
        display = driver.findElement(By.id("display"));
        percent = driver.findElement(By.id("percent"));
        neg = driver.findElement(By.id("neg"));
        clear = driver.findElement(By.id("clear"));
        openParenthesis = driver.findElement(By.id("openParenthesis"));
        closeParenthesis = driver.findElement(By.id("closeParenthesis"));
        memoryRecall = driver.findElement(By.id("memoryRecall"));
        memorySubtract = driver.findElement(By.id("memorySubtract"));
        memoryClear = driver.findElement(By.id("memoryClear"));
        square = driver.findElement(By.id("square"));
        cube = driver.findElement(By.id("cube"));
        pow = driver.findElement(By.id("pow"));
        eToXPower = driver.findElement(By.id("eToXPower"));
        inverse = driver.findElement(By.id("inverse"));
        sqrt = driver.findElement(By.id("sqrt"));
        cbrt = driver.findElement(By.id("cbrt"));
        nthRoot = driver.findElement(By.id("nthRoot"));
        ln = driver.findElement(By.id("ln"));
        log = driver.findElement(By.id("log"));
        factorial = driver.findElement(By.id("factorial"));
        sin = driver.findElement(By.id("sin"));
        cos = driver.findElement(By.id("cos"));
        tan = driver.findElement(By.id("tan"));
        e = driver.findElement(By.id("E"));
        toExponential = driver.findElement(By.id("toExponential"));
        trigUnit = driver.findElement(By.id("trigUnit"));
        sinh = driver.findElement(By.id("sinh"));
        cosh = driver.findElement(By.id("cosh"));
        tanh = driver.findElement(By.id("tanh"));
        pi = driver.findElement(By.id("PI"));
        random = driver.findElement(By.id("random"));
        unit = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/section[1]/div[1]"));
    }


    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }


    @Test
    public void sumOfNatural() {
        one.click();
        add.click();
        two.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(3, actual);
    }

    @Test
    public void sumOfNaturalAndZero() {
        nine.click();
        eight.click();
        six.click();
        add.click();
        zero.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(986, actual);
    }

    @Test
    public void sumOfNaturalFromStart() {
        clear.click();
        add.click();
        five.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(5, actual);
    }

    @Test
    public void subtractOfNatural() {
        four.click();
        subtract.click();
        two.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(2, actual);
    }

    @Test
    public void subtractOfNaturalAndZero() {
        one.click();
        zero.click();
        zero.click();
        zero.click();
        subtract.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1000", display.getText());
    }

    @Test
    public void subtractOfNatural_res_neg() {
        one.click();
        subtract.click();
        two.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(-1, actual);
    }

    @Test
    public void subtractOfNaturalFromStart() {
        clear.click();
        subtract.click();
        nine.click();
        nine.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(-99, actual);
    }

    @Test
    public void divideOfNatural() {
        nine.click();
        divide.click();
        three.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(3, actual);
    }

    @Test
    public void divideOfNatural_res_Real() {
        five.click();
        divide.click();
        two.click();
        equals.click();
        display.getText();
        String actual = display.getText();
        Assert.assertEquals("2.5", actual);
    }

    @Test
    public void divideOfNaturalByZero() {
        one.click();
        zero.click();
        divide.click();
        zero.click();
        equals.click();
        display.getText();
        String actual = display.getText();
        Assert.assertEquals("Infinity", actual);
    }

    @Test
    public void divideOfNaturalByOne() {
        nine.click();
        nine.click();
        divide.click();
        one.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(99, actual);
    }

    @Test
    public void divideOfNaturalFromStart() {
        clear.click();
        divide.click();
        seven.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(0, actual);
    }

    @Test
    public void multiplyOfNatural() {
        five.click();
        multiply.click();
        eight.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(40, actual);
    }

    @Test
    public void multiplyOfNaturalByOne() {
        seven.click();
        two.click();
        multiply.click();
        one.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(72, actual);
    }

    @Test
    public void multiplyOfNaturalByZero() {
        four.click();
        multiply.click();
        zero.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(0, actual);
    }

    @Test
    public void multiplyOfNaturalFromStart() {
        clear.click();
        multiply.click();
        two.click();
        two.click();
        two.click();
        two.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(0, actual);
    }

    @Test
    public void sumOfRealNumbers_res_Natural() {
        decimal.click();
        nine.click();
        add.click();
        decimal.click();
        one.click();
        equals.click();
        display.getText();
        int actual = Integer.parseInt(display.getText());
        Assert.assertEquals(1, actual);
    }

    @Test
    public void sumOfRealNumbers_res_Real() {
        decimal.click();
        nine.click();
        add.click();
        decimal.click();
        zero.click();
        zero.click();
        one.click();
        equals.click();
        display.getText();
        String actual = display.getText();
        Assert.assertEquals("0.901", actual);
    }

    @Test
    public void sumOfRealNumbersAndNatural() {
        nine.click();
        decimal.click();
        five.click();
        add.click();
        one.click();
        equals.click();
        display.getText();
        String actual = display.getText();
        Assert.assertEquals("10.5", actual);
    }

    @Test
    public void sumOfRealNumbersByZero() {
        two.click();
        three.click();
        zero.click();
        decimal.click();
        zero.click();
        seven.click();
        add.click();
        zero.click();
        equals.click();
        display.getText();
        String actual = display.getText();
        Assert.assertEquals("230.07", actual);
    }

    @Test
    public void subtractOfRealNumbers() {
        one.click();
        zero.click();
        zero.click();
        decimal.click();
        five.click();
        three.click();
        subtract.click();
        zero.click();
        decimal.click();
        five.click();
        equals.click();
        display.getText();
        Assert.assertEquals("100.03", display.getText());
    }

    @Test
    public void subtractOfRealNumbers_res_Nat() {
        one.click();
        zero.click();
        zero.click();
        decimal.click();
        five.click();
        three.click();
        subtract.click();
        zero.click();
        decimal.click();
        five.click();
        three.click();
        equals.click();
        display.getText();
        Assert.assertEquals("100", display.getText());
    }

    @Test
    public void subtractOfRealNumbers_neg_res() {
        six.click();
        decimal.click();
        zero.click();
        four.click();
        subtract.click();
        one.click();
        two.click();
        eight.click();
        decimal.click();
        three.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-122.26", display.getText());
    }

    @Test
    public void subtractOfRealNumbersByZero() {
        eight.click();
        zero.click();
        decimal.click();
        zero.click();
        one.click();
        subtract.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("80.01", display.getText());
    }

    @Test
    public void subtractOfRealNumbersAndNatural() {
        one.click();
        decimal.click();
        nine.click();
        subtract.click();
        one.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0.9", display.getText());
    }

    @Test
    public void divideOfRealNumbers() {
        nine.click();
        seven.click();
        five.click();
        one.click();
        decimal.click();
        zero.click();
        eight.click();
        four.click();
        divide.click();
        two.click();
        decimal.click();
        four.click();
        nine.click();
        five.click();
        zero.click();
        four.click();
        equals.click();
        display.getText();
        Assert.assertEquals("3908.1874438886751315", display.getText());
    }

    @Test
    public void divideOfRealNumbersByZero() {
        eight.click();
        decimal.click();
        zero.click();
        one.click();
        divide.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("Infinity", display.getText());
    }

    @Test
    public void divideOfRealNumbersAndNatural() {
        seven.click();
        decimal.click();
        three.click();
        divide.click();
        one.click();
        equals.click();
        display.getText();
        Assert.assertEquals("7.3", display.getText());
    }

    @Test
    public void multiplyOfRealNumbers() {
        decimal.click();
        two.click();
        zero.click();
        one.click();
        multiply.click();
        one.click();
        decimal.click();
        three.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0.2613", display.getText());
    }

    @Test
    public void multiplyOfRealNumbersAndNatural() {
        nine.click();
        decimal.click();
        four.click();
        multiply.click();
        one.click();
        equals.click();
        display.getText();
        Assert.assertEquals("9.4", display.getText());
    }

    @Test
    public void multiplyOfRealNumbersByZero() {
        three.click();
        zero.click();
        decimal.click();
        zero.click();
        zero.click();
        seven.click();
        multiply.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }


    @Test
    public void sumOfNegative() {
        six.click();
        seven.click();
        neg.click();
        add.click();
        three.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-70", display.getText());
    }

    @Test
    public void sumOfNegNaturalAndZero() {
        four.click();
        neg.click();
        add.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-4", display.getText());
    }

    @Test
    public void sumOfNegNaturalAndReal() {
        seven.click();
        zero.click();
        zero.click();
        neg.click();
        add.click();
        decimal.click();
        one.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-699.9", display.getText());
    }

    @Test
    public void subtractOfNegNatural() {
        three.click();
        two.click();
        neg.click();
        subtract.click();
        two.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-30", display.getText());
    }

    @Test
    public void subtractOfNegNaturalAndZero() {
        five.click();
        one.click();
        neg.click();
        subtract.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-51", display.getText());
    }


    @Test
    public void subtractOfNegNaturalAndNegReal() {
        eight.click();
        four.click();
        five.click();
        neg.click();
        subtract.click();
        seven.click();
        decimal.click();
        zero.click();
        zero.click();
        one.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-837.999", display.getText());
    }

    @Test
    public void divideOfNegNatural() {
        nine.click();
        nine.click();
        nine.click();
        nine.click();
        nine.click();
        neg.click();
        divide.click();
        three.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("33333", display.getText());
    }

    @Test
    public void divideOfNegAndPositiveNatural() {
        eight.click();
        neg.click();
        divide.click();
        two.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-4", display.getText());
    }

    @Test
    public void divideOfNegNaturalAndZero() {
        five.click();
        five.click();
        five.click();
        neg.click();
        divide.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-Infinity", display.getText());
    }

    @Test
    public void divideOfNegNaturalAndPositiveReal() {
        seven.click();
        neg.click();
        divide.click();
        two.click();
        decimal.click();
        five.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-2.8", display.getText());
    }


    @Test
    public void multiplyOfNegNatural() {
        two.click();
        zero.click();
        neg.click();
        multiply.click();
        one.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("20", display.getText());
    }

    @Test
    public void multiplyOfNegAndPositiveNatural() {
        seven.click();
        neg.click();
        multiply.click();
        five.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-35", display.getText());
    }

    @Test
    public void multiplyOfNegNaturalAndZero() {
        three.click();
        neg.click();
        multiply.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void multiplyOfNegNaturalAndReal() {
        one.click();
        one.click();
        neg.click();
        multiply.click();
        one.click();
        decimal.click();
        one.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("12.1", display.getText());
    }

    @Test
    public void clearFunction() {
        six.click();
        multiply.click();
        two.click();
        equals.click();
        clear.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void clearFunctionFromStart() {
        clear.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void percentOfNegNatural() {
        eight.click();
        seven.click();
        neg.click();
        percent.click();
        display.getText();
        Assert.assertEquals("-0.87", display.getText());
    }

    @Test
    public void percentOfReal() {
        decimal.click();
        four.click();
        percent.click();
        display.getText();
        Assert.assertEquals("0.004", display.getText());
    }

    @Test
    public void squareOfNatural() {
        one.click();
        zero.click();
        square.click();
        display.getText();
        Assert.assertEquals("100", display.getText());
    }


    @Test
    public void squareOfNegNumber() {
        five.click();
        neg.click();
        square.click();
        display.getText();
        Assert.assertEquals("25", display.getText());
    }

    @Test
    public void squareOfOne() {
        one.click();
        square.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void squareOfZero() {
        zero.click();
        square.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void squareOfRealNumber() {
        eight.click();
        decimal.click();
        zero.click();
        three.click();
        square.click();
        display.getText();
        Assert.assertEquals("64.4809", display.getText());
    }

    @Test
    public void cubeOfNatural() {
        nine.click();
        nine.click();
        nine.click();
        cube.click();
        equals.click();
        display.getText();
        Assert.assertEquals("997002999", display.getText());
    }

    @Test
    public void cubeOfNegNumber() {
        nine.click();
        nine.click();
        neg.click();
        cube.click();
        display.getText();
        Assert.assertEquals("-970299", display.getText());
    }

    @Test
    public void cubeOfOne() {
        one.click();
        cube.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void cubeOfZero() {
        zero.click();
        cube.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void cubeOfRealNumber() {
        six.click();
        decimal.click();
        zero.click();
        five.click();
        cube.click();
        display.getText();
        Assert.assertEquals("221.445125", display.getText());
    }

    @Test
    public void powOfNatural() {
        four.click();
        pow.click();
        five.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1024", display.getText());
    }

    @Test
    public void powOneOfNegNumber() {
        nine.click();
        neg.click();
        pow.click();
        one.click();
        equals.click();
        display.getText();
        Assert.assertEquals("-9", display.getText());
    }

    @Test
    public void powOfOne() {
        one.click();
        pow.click();
        four.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void powOfZero() {
        zero.click();
        pow.click();
        seven.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void powOfRealNumber() {
        two.click();
        decimal.click();
        two.click();
        six.click();
        pow.click();
        nine.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1538.0694722479453558", display.getText());
    }

    @Test
    public void powZero() {
        five.click();
        pow.click();
        zero.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void powNegative() {
        two.click();
        pow.click();
        four.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0.0625", display.getText());
    }

    @Test
    public void powNegativeZero() {
        three.click();
        pow.click();
        zero.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void eToXPowerNatural() {
        two.click();
        eToXPower.click();
        display.getText();
        Assert.assertEquals("7.3890560989306502272", display.getText());
    }

    @Test
    public void eToXPowerZero() {
        zero.click();
        eToXPower.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void eToXPowerNegNumber() {
        four.click();
        neg.click();
        eToXPower.click();
        display.getText();
        Assert.assertEquals("0.018315638888734186637", display.getText());
    }

    @Test
    public void eToXPowerOne() {
        one.click();
        eToXPower.click();
        display.getText();
        Assert.assertEquals("2.718281828459045", display.getText());
    }

    @Test
    public void eToXPowerRealNumber() {
        three.click();
        decimal.click();
        zero.click();
        eight.click();
        eToXPower.click();
        display.getText();
        Assert.assertEquals("21.758402396197077844", display.getText());
    }

    @Test
    public void tenToXPowerZero() {
        zero.click();
        tenToXPower.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void tenToXPowerOne() {
        one.click();
        tenToXPower.click();
        display.getText();
        Assert.assertEquals("10", display.getText());
    }

    @Test
    public void tenToXPowerNatural() {
        four.click();
        tenToXPower.click();
        display.getText();
        Assert.assertEquals("10000", display.getText());
    }


    @Test
    public void tenToXPowerNegNumber() {
        six.click();
        neg.click();
        tenToXPower.click();
        display.getText();
        Assert.assertEquals("0.000001", display.getText());
    }

    @Test
    public void tenToXPowerRealNumber() {
        one.click();
        decimal.click();
        two.click();
        tenToXPower.click();
        display.getText();
        Assert.assertEquals("15.848931924611134852", display.getText());
    }

    @Test
    public void inverseNaturalNumber() {
        nine.click();
        inverse.click();
        display.getText();
        Assert.assertEquals("0.11111111111111111111", display.getText());
    }

    @Test
    public void inverseNegNumber() {
        four.click();
        neg.click();
        inverse.click();
        display.getText();
        Assert.assertEquals("-0.25", display.getText());
    }

    @Test
    public void inverseRealNumber() {
        three.click();
        decimal.click();
        six.click();
        six.click();
        inverse.click();
        display.getText();
        Assert.assertEquals("0.273224043715846994535", display.getText());
    }

    @Test
    public void inverseZero() {
        zero.click();
        inverse.click();
        display.getText();
        Assert.assertEquals("Infinity", display.getText());
    }

    @Test
    public void inverseOne() {
        one.click();
        inverse.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }


    @Test
    public void inverseFromStart() {
        clear.click();
        inverse.click();
        display.getText();
        Assert.assertEquals("Infinity", display.getText());
    }

    @Test
    public void squareRootFromStart() {
        clear.click();
        sqrt.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }


    @Test
    public void squareRootNaturalNumber() {
        four.click();
        sqrt.click();
        display.getText();
        Assert.assertEquals("2", display.getText());
    }

    @Test
    public void squareRootNegNumber() {
        nine.click();
        neg.click();
        sqrt.click();
        display.getText();
        Assert.assertEquals("NaN", display.getText());
    }

    @Test
    public void squareRootRealNumber() {
        eight.click();
        decimal.click();
        three.click();
        two.click();
        sqrt.click();
        display.getText();
        Assert.assertEquals("2.8844410203711914345", display.getText());
    }

    @Test
    public void squareRootZero() {
        zero.click();
        sqrt.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void squareRootOne() {
        one.click();
        sqrt.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void cubeRootFromStart() {
        clear.click();
        cbrt.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }


    @Test
    public void cubeRootNaturalNumber() {
        eight.click();
        cbrt.click();
        display.getText();
        Assert.assertEquals("2", display.getText());
    }

    @Test
    public void cubeRootNegNumber() {
        one.click();
        zero.click();
        neg.click();
        cbrt.click();
        display.getText();
        Assert.assertEquals("-2.1544346900318837218", display.getText());
    }

    @Test
    public void cubeRootRealNumber() {
        six.click();
        decimal.click();
        two.click();
        cbrt.click();
        display.getText();
        Assert.assertEquals("1.8370905500142277207", display.getText());
    }

    @Test
    public void cubeRootZero() {
        zero.click();
        cbrt.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void cubeRootOne() {
        one.click();
        cbrt.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void nthRootNaturalNumber() {
        one.click();
        zero.click();
        zero.click();
        zero.click();
        zero.click();
        zero.click();
        nthRoot.click();
        five.click();
        equals.click();
        display.getText();
        Assert.assertEquals("10", display.getText());
    }

    @Test
    public void nthRootNegRoot() {
        nine.click();
        nthRoot.click();
        three.click();
        neg.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0.4807498567691361274", display.getText());
    }

    @Test
    public void nthRootNegNumber() {
        nine.click();
        neg.click();
        nthRoot.click();
        six.click();
        equals.click();
        display.getText();
        Assert.assertEquals("NaN", display.getText());
    }


    @Test
    public void nthRootRealNumber() {
        seven.click();
        decimal.click();
        four.click();
        nthRoot.click();
        five.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1,4922663431747894279", display.getText());
    }

    @Test
    public void nthRootZero() {
        zero.click();
        nthRoot.click();
        seven.click();
        equals.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void nthRootOne() {
        one.click();
        nthRoot.click();
        nine.click();
        one.click();
        equals.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void logarithmNatFromStart() {
        clear.click();
        ln.click();
        display.getText();
        Assert.assertEquals("-Infinity", display.getText());
    }

    @Test
    public void logarithmNatNaturalNumber() {
        one.click();
        zero.click();
        zero.click();
        zero.click();
        nine.click();
        ln.click();
        display.getText();
        Assert.assertEquals("9.2112399672190188291", display.getText());
    }

    @Test
    public void logarithmNatNegNumber() {
        five.click();
        neg.click();
        ln.click();
        display.getText();
        Assert.assertEquals("NaN", display.getText());
    }

    @Test
    public void logarithmNatRealNumber() {
        six.click();
        decimal.click();
        two.click();
        ln.click();
        display.getText();
        Assert.assertEquals("1.8245492920510458713", display.getText());
    }

    @Test
    public void logarithmNatZero() {
        zero.click();
        ln.click();
        display.getText();
        Assert.assertEquals("-Infinity", display.getText());
    }

    @Test
    public void logarithmNatOne() {
        one.click();
        ln.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void logarithmFromStart() {
        clear.click();
        log.click();
        display.getText();
        Assert.assertEquals("-Infinity", display.getText());
    }

    @Test
    public void logarithmNaturalNumber() {
        one.click();
        zero.click();
        zero.click();
        zero.click();
        nine.click();
        log.click();
        display.getText();
        Assert.assertEquals("4.000390689249910131", display.getText());
    }

    @Test
    public void logarithmNegNumber() {
        five.click();
        neg.click();
        log.click();
        display.getText();
        Assert.assertEquals("NaN", display.getText());
    }

    @Test
    public void logarithmRealNumber() {
        six.click();
        decimal.click();
        two.click();
        log.click();
        display.getText();
        Assert.assertEquals("0.79239168949825387488", display.getText());
    }

    @Test
    public void logarithmZero() {
        zero.click();
        log.click();
        display.getText();
        Assert.assertEquals("-Infinity", display.getText());
    }

    @Test
    public void logarithmOne() {
        one.click();
        log.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void factorialFromStart() {
        clear.click();
        factorial.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void factorialNaturalNumber() {
        one.click();
        zero.click();
        factorial.click();
        display.getText();
        Assert.assertEquals("3628800", display.getText());
    }

    @Test
    public void factorialNegNumber() {
        five.click();
        neg.click();
        factorial.click();
        display.getText();
        Assert.assertEquals("NaN", display.getText());
    }

    @Test
    public void factorialRealNumber() {
        six.click();
        decimal.click();
        two.click();
        factorial.click();
        display.getText();
        Assert.assertEquals("1050.3178166626825977004", display.getText());
    }

    @Test
    public void factorialZero() {
        zero.click();
        factorial.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void factorialOne() {
        one.click();
        factorial.click();
        display.getText();
        Assert.assertEquals("1", display.getText());
    }

    @Test
    public void numberToNeg() {
        clear.click();
        one.click();
        neg.click();
        display.getText();
        Assert.assertEquals("-1", display.getText());
    }

    @Test
    public void numberToNeg_Zero() {
        clear.click();
        zero.click();
        neg.click();
        display.getText();
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void sinFromStartDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            clear.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            clear.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void sinNaturalNumberDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            one.click();
            zero.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            one.click();
            zero.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0.17364817766693034885", display.getText());
    }

    @Test
    public void sinNegNumberDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            five.click();
            neg.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            five.click();
            neg.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("-0.0871557427476581736", display.getText());
    }

    @Test
    public void sinRealNumberDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            six.click();
            decimal.click();
            two.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            six.click();
            decimal.click();
            two.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0.10799935570602285122", display.getText());
    }

    @Test
    public void sinZeroDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            zero.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            zero.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void sinOneDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            one.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            one.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0.017452406437283512819", display.getText());
    }


    @Test
    public void sinFromStartRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void sinNaturalNumberRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            one.click();
            zero.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            one.click();
            zero.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("-0.5440211108893698134", display.getText());
    }

    @Test
    public void sinNegNumberRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            five.click();
            neg.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            five.click();
            neg.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0.95892427466313846889", display.getText());
    }

    @Test
    public void sinRealNumberRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            six.click();
            decimal.click();
            two.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            six.click();
            decimal.click();
            two.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("-0.083089402817496578001", display.getText());
    }

    @Test
    public void sinZeroRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            zero.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            zero.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0", display.getText());
    }

    @Test
    public void sinOneRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            one.click();
            sin.click();
            display.getText();
        } else {
            trigUnit.click();
            one.click();
            sin.click();
            display.getText();
        }
        Assert.assertEquals("0.84147098480789650665", display.getText());
    }

    @Test
    public void expressionInBrackets() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            clear.click();
            four.click();
            multiply.click();
            openParenthesis.click();
            one.click();
            add.click();
            two.click();
            closeParenthesis.click();
            equals.click();
            display.getText();
        } else {
            trigUnit.click();
            clear.click();
            four.click();
            multiply.click();
            openParenthesis.click();
            one.click();
            add.click();
            two.click();
            closeParenthesis.click();
            equals.click();
            display.getText();
        }
        Assert.assertEquals("12", display.getText());
    }

    @Test
    public void eConstanta() {
        clear.click();
        e.click();
        display.getText();
        Assert.assertEquals("2.718281828459045", display.getText());
    }

    @Test
    public void eConstantaOperations() {
        clear.click();
        e.click();
        multiply.click();
        two.click();
        equals.click();
        display.getText();
        Assert.assertEquals("5.43656365691809", display.getText());
    }

    @Test
    public void piNumber() {
        clear.click();
        pi.click();
        display.getText();
        Assert.assertEquals("3.1415926535897932385", display.getText());
    }

    @Test
    public void piNumberOperation() {
        clear.click();
        pi.click();
        subtract.click();
        one.click();
        equals.click();
        display.getText();
        Assert.assertEquals("2.1415926535897932385", display.getText());
    }


    @Test
    public void cosInRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            four.click();
            five.click();
            cos.click();
            display.getText();
        } else {
            trigUnit.click();
            four.click();
            five.click();
            cos.click();
            display.getText();
        }
        Assert.assertEquals("0.52532198881772969605", display.getText());
    }

    @Test
    public void cosInDeg_Neg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            one.click();
            zero.click();
            neg.click();
            cos.click();
            display.getText();
        } else {
            trigUnit.click();
            one.click();
            zero.click();
            neg.click();
            cos.click();
            display.getText();
        }
        Assert.assertEquals("0.98480775301220805937", display.getText());
    }

    @Test
    public void tanInRad_RealNumber() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            three.click();
            four.click();
            decimal.click();
            one.click();
            two.click();
            tan.click();
            display.getText();
        } else {
            trigUnit.click();
            three.click();
            four.click();
            decimal.click();
            one.click();
            two.click();
            tan.click();
            display.getText();
        }
        Assert.assertEquals("-0.46775341326068876", display.getText());
    }

    @Test
    public void sinhInDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            three.click();
            sinh.click();
            display.getText();
        } else {
            trigUnit.click();
            three.click();
            sinh.click();
            display.getText();
        }
        Assert.assertEquals("0.0523838054357798467692354898979", display.getText());
    }

    @Test
    public void sinhInRad() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            three.click();
            sinh.click();
            display.getText();
        } else {
            trigUnit.click();
            three.click();
            sinh.click();
            display.getText();
        }
        Assert.assertEquals("10.017874927409901899", display.getText());
    }

    @Test
    public void coshInRad_RealNumber() {
        unit.getText();
        if (unit.getText().equals("rad")) {
            clear.click();
            decimal.click();
            one.click();
            cosh.click();
            display.getText();
        } else {
            trigUnit.click();
            clear.click();
            decimal.click();
            one.click();
            cosh.click();
            display.getText();
        }
        Assert.assertEquals("1.000001523087485565", display.getText());
    }

    @Test
    public void tanhInDeg() {
        unit.getText();
        if (unit.getText().equals("deg")) {
            clear.click();
            five.click();
            tanh.click();
            display.getText();
        } else {
            trigUnit.click();
            clear.click();
            five.click();
            tanh.click();
            display.getText();
        }
        Assert.assertEquals("0.08704561128797016701", display.getText());
    }
}








