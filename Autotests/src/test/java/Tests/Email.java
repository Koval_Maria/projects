package Tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Email {

    private static ChromeDriver driver;
    private static WebDriverWait waiter;

    @BeforeClass
    public static void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://webmail.meta.ua/");
        waiter = new WebDriverWait(driver, 2000);
        authorize();
    }

    private static void authorize() {
        driver.findElement(By.id("login-field")).sendKeys("tester223@meta.ua");
        driver.findElement(By.id("pass-field")).sendKeys("porosia");
        WebElement btn = driver.findElement(By.id("loginbtnua"));
        btn.click();
    }

    @Test
    public void test_userNameExisted() {
        String actual = driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td/div/div[1]/div[1]/span/strong")).getText();
        String expected = "tester223@meta.ua";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkMessagesQuantityBefore() {
        WebElement container = waiter.until(ExpectedConditions.elementToBeClickable(By.className("left_panel_block")));
        String actual = container.findElement(By.cssSelector("#left > div:nth-child(3) > div.left_boxes > div.treeSelector > div > div > div.treeElement.grayFon")).getText();
        String expected = "0";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void writeMessage() {
        WebElement btnMakeMessage = driver.findElement(By.id("id_send_email"));
        btnMakeMessage.click();

        WebElement fieldTo = driver.findElement(By.className("dline_container"));
        WebElement textAreaTo = fieldTo.findElement(By.cssSelector("#send_to"));
        textAreaTo.sendKeys("tester223@meta.ua");

        WebElement container = waiter.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/table/tbody/tr[1]/td/table/tbody/tr/td[2]/form/div/div[2]/div/div[2]/div[1]/div[3]/div/textarea")));
        WebElement textArea = container.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td/table/tbody/tr/td[2]/form/div/div[2]/div/div[2]/div[1]/div[3]/div/textarea"));
        textArea.sendKeys("HELLO MELLO");

        WebElement btnSend = driver.findElement(By.cssSelector("input[name='send']"));
        btnSend.click();

        WebElement container1 = waiter.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#container > table > tbody > tr:nth-child(1) > td > table > tbody > tr")));
       String actual = container1.findElement(By.cssSelector("#send_ok > span")).getText();
        WebElement cross = container1.findElement(By.id("close_send_ok"));
        cross.click();

        String expected = "Ваше повідомлення надiслано";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkMessagesQuantityAfter() {
        waiter.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#id_delete")));
//        driver.navigate().refresh();
        WebElement container = waiter.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#left > div:nth-child(3)")));

        String actual = container.findElement(By.cssSelector("#left > div:nth-child(3) > div.left_boxes > div.treeSelector > div > div > div.treeElement.grayFon")).getText();
        String expected = "1 / 1";
        Assert.assertEquals(expected, actual);
    }
}