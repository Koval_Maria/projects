package Tests;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class Converter {
    static WebElement fromUnit;
    static WebElement toUnit;
    static WebElement field;
    static WebElement convert;
    static WebElement result;
    static WebElement formula;
    static WebElement toMeters;
    static WebElement toKilometers;
    static WebElement toMiles;
    static WebElement toFeet;
    static WebElement toYards;
    static WebElement fromMeters;
    static WebElement fromKilometers;
    static WebElement fromMiles;
    static WebElement fromFeet;
    static WebElement fromYards;


    private static WebDriver driver;

    @BeforeClass
    public static void initDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Mary\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/belichenko/converterLength/index.html");

        fromUnit = driver.findElement(By.name("fromUnit"));
        toUnit = driver.findElement(By.name("toUnit"));
        field = driver.findElement(By.name("fromValue"));
        convert = driver.findElement(By.className("yah"));
        result = driver.findElement(By.id("toValue"));
        formula = driver.findElement(By.id("formula"));
        toMeters = driver.findElement(By.xpath("/html/body/form/p[3]/select/option[1]"));
        toKilometers = driver.findElement(By.xpath("/html/body/form/p[3]/select/option[2]"));
        toMiles = driver.findElement(By.xpath("/html/body/form/p[3]/select/option[3]"));
        toFeet = driver.findElement(By.xpath("/html/body/form/p[3]/select/option[4]"));
        toYards = driver.findElement(By.xpath("/html/body/form/p[3]/select/option[5]"));
        fromMeters = driver.findElement(By.xpath("/html/body/form/select/option[1]"));
        fromKilometers = driver.findElement(By.xpath("/html/body/form/select/option[2]"));
        fromMiles = driver.findElement(By.xpath("/html/body/form/select/option[3]"));
        fromFeet = driver.findElement(By.xpath("/html/body/form/select/option[4]"));
        fromYards = driver.findElement(By.xpath("/html/body/form/select/option[5]"));

    }


    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }

    @Test
    public void emptyField() {
        field.click();
        field.clear();
        convert.click();
        result.getText();
        Assert.assertEquals("0", result.getText());
    }

    @Test
    public void invalidSymbols_parentheses() {
        field.click();
        field.clear();
        field.sendKeys("(9)");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }

    @Test
    public void invalidSymbols_brackets() {
        field.click();
        field.clear();
        field.sendKeys("[8]");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }

    @Test
    public void invalidSymbols_braces() {
        field.click();
        field.clear();
        field.sendKeys("{8}");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }

    @Test
    public void invalidSymbols_punctuationMarks() {
        field.click();
        field.clear();
        field.sendKeys("1!,.?/;:\"'`");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }

    @Test
    public void invalidSymbols() {
        field.click();
        field.clear();
        field.sendKeys("4$%#№&@><|\\/^");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }


    @Test
    public void invalidSymbols_space() {
        field.click();
        field.clear();
        field.sendKeys("1 0");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }

    @Test
    public void invalidSymbols_mathOperators() {
        field.click();
        field.clear();
        field.sendKeys("2+3/1*4-2=0");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }

    @Test
    public void formulaOfMetersToMeters() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMeters.click();
        formula.getText();
        Assert.assertEquals("meters = 1 meters", formula.getText());
    }

    @Test
    public void formulaOfMetersToKilometers() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toKilometers.click();
        formula.getText();
        Assert.assertEquals("meters = 0.001 kilometers", formula.getText());
    }

    @Test
    public void formulaOfMetersToMiles() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMiles.click();
        formula.getText();
        Assert.assertEquals("meters = 0.000621 miles", formula.getText());
    }

    @Test
    public void formulaOfMetersToFeet() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toFeet.click();
        formula.getText();
        Assert.assertEquals("meters = 3.28084 feet", formula.getText());
    }

    @Test
    public void formulaOfMetersToYards() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toYards.click();
        formula.getText();
        Assert.assertEquals("meters = 1.09361 yards", formula.getText());
    }

    @Test
    public void formulaOfKilometersToMeters() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toMeters.click();
        formula.getText();
        Assert.assertEquals("kilometers = 1000 meters", formula.getText());
    }

    @Test
    public void formulaOfKilometersToKilometers() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toKilometers.click();
        formula.getText();
        Assert.assertEquals("kilometers = 1 kilometers", formula.getText());
    }

    @Test
    public void formulaOfKilometersToMiles() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toMiles.click();
        formula.getText();
        Assert.assertEquals("kilometers = 0.621 miles", formula.getText());
    }

    @Test
    public void formulaOfKilometersToFeet() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toFeet.click();
        formula.getText();
        Assert.assertEquals("kilometers = 3280.84 feet", formula.getText());
    }

    @Test
    public void formulaOfKilometersToYards() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toYards.click();
        formula.getText();
        Assert.assertEquals("kilometers = 1093.61 yards", formula.getText());
    }

    @Test
    public void formulaOfMilesToMeters() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toMeters.click();
        formula.getText();
        Assert.assertEquals("miles = 1609.344 meters", formula.getText());
    }

    @Test
    public void formulaOfMilesToKilometers() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toKilometers.click();
        formula.getText();
        Assert.assertEquals("miles = 1.60934 kilometers", formula.getText());
    }

    @Test
    public void formulaOfMilesToMiles() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toMiles.click();
        formula.getText();
        Assert.assertEquals("miles = 1 miles", formula.getText());
    }

    @Test
    public void formulaOfMilesToFeet() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toFeet.click();
        formula.getText();
        Assert.assertEquals("miles = 5280 feet", formula.getText());
    }

    @Test
    public void formulaOfMilesToYards() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toYards.click();
        formula.getText();
        Assert.assertEquals("miles = 1760 yards", formula.getText());
    }

    @Test
    public void formulaOfFeetToMeters() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toMeters.click();
        formula.getText();
        Assert.assertEquals("feet = 0.3048 meters", formula.getText());
    }

    @Test
    public void formulaOfFeetToKilometers() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toKilometers.click();
        formula.getText();
        Assert.assertEquals("feet = 0.0003048 kilometers", formula.getText());
    }

    @Test
    public void formulaOfFeetToMiles() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toMiles.click();
        formula.getText();
        Assert.assertEquals("feet = 0.000189394 miles", formula.getText());
    }

    @Test
    public void formulaOfFeetToFeet() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toFeet.click();
        formula.getText();
        Assert.assertEquals("feet = 1 feet", formula.getText());
    }

    @Test
    public void formulaOfFeetToYards() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toYards.click();
        formula.getText();
        Assert.assertEquals("feet = 0.333333 yards", formula.getText());
    }

    @Test
    public void formulaOfYardsToMeters() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toMeters.click();
        formula.getText();
        Assert.assertEquals("yards = 0.9144 meters", formula.getText());
    }

    @Test
    public void formulaOfYardsToKilometers() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toKilometers.click();
        formula.getText();
        Assert.assertEquals("yards = 0.0009144 kilometers", formula.getText());
    }

    @Test
    public void formulaOfYardsToMiles() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toMiles.click();
        formula.getText();
        Assert.assertEquals("yards = 0.000568182 miles", formula.getText());
    }

    @Test
    public void formulaOfYardsToFeet() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toFeet.click();
        formula.getText();
        Assert.assertEquals("yards = 3 feet", formula.getText());
    }

    @Test
    public void formulaOfYardsToYards() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toYards.click();
        formula.getText();
        Assert.assertEquals("yards = 1 yards", formula.getText());
    }

    @Test
    public void metersToMetersNaturalNumber() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("2");
        convert.click();
        result.getText();
        Assert.assertEquals("2", result.getText());
    }

    @Test
    public void metersToMetersNaturalZero() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("0");
        convert.click();
        result.getText();
        Assert.assertEquals("0", result.getText());
    }

    @Test
    public void metersToMetersNaturalZeroBeforeNumber() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("00091");
        convert.click();
        result.getText();
        Assert.assertEquals("91", result.getText());
    }

    @Test
    public void metersToMetersNegNumber() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("-199");
        convert.click();
        result.getText();
        Assert.assertEquals("-199", result.getText());
    }


    @Test
    public void metersToMetersRealNumberDot() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("0.00019");
        convert.click();
        result.getText();
        Assert.assertEquals("0.00019", result.getText());
    }

    @Test
    public void metersToMetersRealNumberComma() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("0,00019");
        convert.click();
        result.getText();
        Assert.assertEquals("Not a valid number.", result.getText());
    }


    @Test
    public void metersToKilometers() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toKilometers.click();
        field.click();
        field.clear();
        field.sendKeys("100");
        convert.click();
        result.getText();
        Assert.assertEquals("0.1", result.getText());
    }

    @Test
    public void metersToMiles() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toMiles.click();
        field.click();
        field.clear();
        field.sendKeys("23.5");
        convert.click();
        result.getText();
        Assert.assertEquals("0.0145935", result.getText());
    }

    @Test
    public void metersToFeet() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toFeet.click();
        field.click();
        field.clear();
        field.sendKeys("2");
        convert.click();
        result.getText();
        Assert.assertEquals("6.56168", result.getText());
    }

    @Test
    public void metersToYards() {
        fromUnit.click();
        fromMeters.click();
        toUnit.click();
        toYards.click();
        field.click();
        field.clear();
        field.sendKeys("0.3");
        convert.click();
        result.getText();
        Assert.assertEquals("0.32808299999999996", result.getText());
    }


    @Test
    public void kilometersToMeters() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("9000");
        convert.click();
        result.getText();
        Assert.assertEquals("9000000", result.getText());
    }

    @Test
    public void kilometersToMiles() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toMiles.click();
        field.click();
        field.clear();
        field.sendKeys("9");
        convert.click();
        result.getText();
        Assert.assertEquals("5.589", result.getText());
    }

    @Test
    public void kilometersToFeet() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toFeet.click();
        field.click();
        field.clear();
        field.sendKeys("0.8");
        convert.click();
        result.getText();
        Assert.assertEquals("2624.6720000000005", result.getText());
    }

    @Test
    public void kilometersToYards() {
        fromUnit.click();
        fromKilometers.click();
        toUnit.click();
        toYards.click();
        field.click();
        field.clear();
        field.sendKeys("5");
        convert.click();
        result.getText();
        Assert.assertEquals("5468.049999999999", result.getText());
    }


    @Test
    public void milesToMeters() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("2");
        convert.click();
        result.getText();
        Assert.assertEquals("3218.69", result.getText());
    }

    @Test
    public void milesToMiles() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toMiles.click();
        field.click();
        field.clear();
        field.sendKeys("8777");
        convert.click();
        result.getText();
        Assert.assertEquals("8777", result.getText());
    }

    @Test
    public void milesToFeet() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toFeet.click();
        field.click();
        field.clear();
        field.sendKeys("93.43");
        convert.click();
        result.getText();
        Assert.assertEquals("493310.4", result.getText());
    }

    @Test
    public void milesToYards() {
        fromUnit.click();
        fromMiles.click();
        toUnit.click();
        toYards.click();
        field.click();
        field.clear();
        field.sendKeys("0.4");
        convert.click();
        result.getText();
        Assert.assertEquals("704", result.getText());
    }

    @Test
    public void feetToMeters() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("1.5");
        convert.click();
        result.getText();
        Assert.assertEquals("0.45720000000000005", result.getText());
    }

    @Test
    public void feetToMiles() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toMiles.click();
        field.click();
        field.clear();
        field.sendKeys("1999");
        convert.click();
        result.getText();
        Assert.assertEquals("0.378598484", result.getText());
    }

    @Test
    public void feetToFeet() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toFeet.click();
        field.click();
        field.clear();
        field.sendKeys("2");
        convert.click();
        result.getText();
        Assert.assertEquals("2", result.getText());
    }

    @Test
    public void feetToYards() {
        fromUnit.click();
        fromFeet.click();
        toUnit.click();
        toYards.click();
        field.click();
        field.clear();
        field.sendKeys("127");
        convert.click();
        result.getText();
        Assert.assertEquals("42.333333333333", result.getText());
    }


    @Test
    public void yardsToMeters() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toMeters.click();
        field.click();
        field.clear();
        field.sendKeys("0.0001");
        convert.click();
        result.getText();
        Assert.assertEquals("0.00009144", result.getText());
    }

    @Test
    public void yardsToMiles() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toMiles.click();
        field.click();
        field.clear();
        field.sendKeys("65");
        convert.click();
        result.getText();
        Assert.assertEquals("0.03693182 ", result.getText());
    }

    @Test
    public void yardsToFeet() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toFeet.click();
        field.click();
        field.clear();
        field.sendKeys("0.4");
        convert.click();
        result.getText();
        Assert.assertEquals("1.2000000000000002", result.getText());
    }

    @Test
    public void yardsToYards() {
        fromUnit.click();
        fromYards.click();
        toUnit.click();
        toYards.click();
        field.click();
        field.clear();
        field.sendKeys("100");
        convert.click();
        result.getText();
        Assert.assertEquals("100", result.getText());
    }
}







