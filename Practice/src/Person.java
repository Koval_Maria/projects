import java.util.Arrays;
import java.util.UUID;

public class Person {
    private UUID id;
    private String name;
    private int age;
    private Friend[] friends;
    private Brain brain;


    Person(UUID id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    Person(UUID id, String name, int age, int IQ, Friend[] friends) {
        this(id, name, age);
        this.friends = friends; //new Friend[] {}
        this.brain = new Brain(IQ);
    }

    private class Brain {
        private int IQ;

       public Brain(int IQ) {
            if (IQ >= 1 && IQ <= 10) {
                this.IQ = IQ;
            }
            else {
                this.IQ = 5;
            }
        }
    }
//    @Override
//    public String toString() {
//
//        return "ID is " + id + ", " + "name is " + name + ", " + "age is " + age + ", " + "IQ is " + brain.IQ + ", " + Arrays.toString(friends);
//    }
}

