
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

public class Practice {
    public static void main(String[] args) {

        User[] myArray = new User[10];

        for (int i = 0; i < myArray.length; i++) {
            if (i % 2 == 0) {
                myArray[i] = new User(UUID.randomUUID(), new int[]{1, 2, 3}, "ADMIN");
            } else {
                myArray[i] = new User(UUID.randomUUID(), new int[]{4, 5, 6});
            }
        }
        for (int i = 0; i < myArray.length; i++) {
            System.out.println(myArray[i]);
        }
    }

//        Person[] arrayPersons = new Person[10];
//
//        for (int i = 1; i <= arrayPersons.length; i++) {
//            if (i <= 5) {
//                Friend[] friends = new Friend[] {new Friend("male", true), new Friend("female", true)};
//                arrayPersons[i] = new Person(UUID.randomUUID(), "Nike", 18, i, friends);
//            }
//             else {
//                Friend[] friends = new Friend[] {new Friend("male"), new Friend("female")};
//                arrayPersons[i] = new Person(UUID.randomUUID(), "Rose", 18, i, friends);
//            }
//        }
//
//        for (int i = 0; i < arrayPersons.length; i++) {
//            System.out.println(arrayPersons[i]);
//        }
//    }
}
