import java.util.Arrays;
import java.util.UUID;

public class User {


    private UUID id;
    private int[] sessions;
    private String role;


    User(UUID id, int[] sessions, String role) {
        this.id = id;
        this.sessions = sessions;
        this.role = role;

    }

    User(UUID id, int[] sessions) {
        this.id = id;
        this.sessions = sessions;
        this.role = "Default";
    }


    @Override
    public String toString() {

        return "ID is " + id + ", " + "session" + Arrays.toString(sessions) + ", " + role;
    }
}
