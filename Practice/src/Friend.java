import java.util.Arrays;

public class Friend {
    private String gender;
    private boolean isEnemy;

   public Friend(String gender, boolean isEnemy) {
        this.gender = gender;
        this.isEnemy = isEnemy;
    }

    public Friend(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
       return "friend's gender is " + gender + ". " + "Is enemy? " + isEnemy;
    }
}